<?php

/*
Plugin Name: WebCodium TinyMCE Shortcodes Plugin
Description: A WordPress plugin that will add a dropdown in the tinyMCE editor to add shortcodes
Plugin URI: http://webcodium.com
Author: Yura Zhunkivskyi <yura.zh@webcodium.com>
Author URI: http://webcodium.com
Version: 1.0
License: GPL2
*/

class WebCodiumMceShortcodes {

    const SHORTCODES_FILE_NAME = 'shortcodes.yml';

    private $default_shortcodes = ['audio', 'wp_caption', 'caption', 'embed', 'gallery', 'video', 'playlist'];

    private $shortcode_groups = [];

    public function __construct(){
        $this->shortcode_groups = yaml_parse_file(dirname(__FILE__) . '/' . self::SHORTCODES_FILE_NAME);
        $this->mergeTemplateShortcodes();

        add_action('admin_init', array($this, 'initMcePlugin'));
        add_action('admin_footer', array($this, 'getShortcodesArrayScript'));
    }

    /**
     * Merge template shordcodes if template contains shortcodes.yml file
     */
    protected function mergeTemplateShortcodes(){
        $template_file = get_template_directory() . '/' . self::SHORTCODES_FILE_NAME;
        if (file_exists($template_file)) {
            $template_shordcodes = yaml_parse_file($template_file);
            $this->shortcode_groups['groups'] = array_merge($this->shortcode_groups['groups'], $template_shordcodes['groups']);
        }
    }

    public function initMcePlugin(){
        if (current_user_can('edit_posts') && current_user_can('edit_pages')) {
            add_filter('mce_buttons', array($this, 'registerMceButton'));
            add_filter('mce_external_plugins', array($this, 'registerMcePlugin'));
        }
    }

    public function getShortcodesArrayScript(){
        global $shortcode_tags;

        echo '<script type="text/javascript">
        var shortcodes_list = new Array();';

        $count = 0;
        $groups = $this->shortcode_groups['groups'];
        foreach ($groups as $shortcode_group) {
            echo "shortcodes_list[{$count}] = {tag:'{$shortcode_group['name']}', code:' '};";
            $count++;
            foreach ($shortcode_group['value'] as $item) {
                $value = str_replace("\n", "", $item['value']);
                echo "shortcodes_list[{$count}] = {tag:'{$item['name']}', code:'{$value}'};";
                $count++;
            }
        }

        echo '</script>';
    }

    public function registerMceButton($buttons){
        array_push($buttons, 'separator', 'webcodiumShortcodes');
        return $buttons;
    }

    public function registerMcePlugin($plugin_array){
        $plugin_array['webcodiumShortcodes'] = plugins_url('/js/mce-shortcodes.js', __FILE__);

        return $plugin_array;
    }
}

new WebCodiumMceShortcodes();
