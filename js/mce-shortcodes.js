tinymce.PluginManager.add('webcodiumShortcodes', function (editor) {
    var shortcodeValues = [];
    jQuery.each(shortcodes_list, function (i, v) {
        shortcodeValues.push({text: v.tag, value: v.code});
    });
    // console.log(shortcodeValues);
    editor.addButton('webcodiumShortcodes', {
        type: 'listbox',
        text: 'Shortcodes',
        onselect: function (e) {
            var content = this.value();
            if (typeof content !== 'undefined' && content.length !== 0) {
                tinymce.execCommand('mceInsertContent', false, content);
            }
        },
        values: shortcodeValues
    });
});
